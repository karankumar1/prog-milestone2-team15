﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($" -----------------MAIN MENU---------------- ");
            Console.WriteLine($"********************************************");
            Console.WriteLine($"*                                          *");
            Console.WriteLine($"*           1.Date Calculator              *");
            Console.WriteLine($"*           2.Calculate Grade Average      *");
            Console.WriteLine($"*           3.Generate A Random Number     *");
            Console.WriteLine($"*           4.Rate Favourite Food          *");
            Console.WriteLine($"*                                          *");
            Console.WriteLine($"********************************************");
            Console.WriteLine($"   Please select a number and press enter   ");

            var ans = Console.ReadLine();
            int choice = 0;
            if (int.TryParse(ans, out choice))
            {
                switch (choice)
                {
                    case 1:
                        // Baljinder's code here
                        Datecalc();
                        Years();
                        break;

                    case 2:
                        //Sanket's code here
                        GradeAverage();

                        break;

                    case 3:
                        //Terrence's code here

                        break;

                    case 4:
                        //Karan's code here
                        FavFood();

                        break;

                    default:

                        break;
                }
            }
            else
            {
                Console.WriteLine("You must type numeric value only!!! \n" +
                "Press any key for exit");
                Console.ReadKey();
            }
        }
        static void Datecalc()
        { // Baljinder's method starts here 
            var date = DateTime.Today;

            Console.WriteLine($"Please enter your date of birth! (dd/mm/yyyy)");
            DateTime dob = DateTime.Parse(Console.ReadLine());
            Console.WriteLine($"Your date of birth is {dob}");

            Console.WriteLine($" Today's date is {date}");
            var days = date - dob;
            Console.WriteLine($"You are {days} days old now.");
        }
        static void Years()
        {
            int number;
            Console.WriteLine($" Type any number of year");
            number = int.Parse(Console.ReadLine());
            // Baljinder's method ends here
        }
        static void GradeAverage()
        { // sanket's method here
            string level;
            string papers;
            int studentID;
            int paperCode;
            int firstNumber;
            int secondNumber;
            int thirdNumber;
            int Sum;



            Console.Write("Please enter What level(level5 or level6) Your are?: ");
            level = Console.ReadLine();


            Console.Write("Please enter three papers marks With comma and full stop  Your are enrolled in level{0} ?: ", level);
            papers = Console.ReadLine();

            Console.Write("Please enter First Marks: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Second Marks: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Third Marks: ");
            thirdNumber = Convert.ToInt32(Console.ReadLine());


            Console.Write("Please enter Your Student ID!:  ");
            studentID = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Your PaperCode:  ");
            paperCode = Convert.ToInt32(Console.ReadLine());

            Sum = (firstNumber + secondNumber + thirdNumber) / 3;
            Console.ReadLine();

            Console.WriteLine("Hello, here  are the details of your level, papers, and Mark . ON Your Leve {0}, IN  {1}, with Student ID {2},  Your Papercode  is {3}, Your Final marks that you got {4},%marks", level, papers, studentID, paperCode, Sum);

            Console.ReadLine();

            if (Sum >= 50)
            {
                Console.WriteLine($"Great you have passed!");

            }

            else
            {
                Console.WriteLine($"Sorry you are failed!");
            }

            Console.ReadLine();
        }
        static void FavFood()
        { // karan's code
            string a;
            string b;
            string c;
            string d;
            string e;
            int orange;
            int carrot;
            int musroom;
            int banana;
            int rate;
            int rate2;
            int rate3;
            int rate4;
            int rate5;
            string value;



            Console.Write("please enter your first five fruits that you like and your most like will be your list number  ");
            Console.ReadLine();


            //Console.Write("please enter the how many apples do you like  ");
            //apple = Convert.ToInt32(Console.ReadLine());

            //Console.Write("please enter the how many orange do you like  ");
            //orange = Convert.ToInt32(Console.ReadLine());

            //Console.Write("please enter the how many carrot do you like  ");
            //carrot = Convert.ToInt32(Console.ReadLine());

            //Console.Write("please enter the how many musroom do you like  ");
            //musroom = Convert.ToInt32(Console.ReadLine());

            //Console.Write("please enter the how many banana do you like  ");
            //banana = Convert.ToInt32(Console.ReadLine());

            Console.Write(" Enter your first fruits   ");
            a = Console.ReadLine();

            //  Console.ReadLine();
            Console.Write(" Please Rate it   ");
            rate = Convert.ToInt32(Console.ReadLine());
            //  Console.ReadLine();

            Console.Write(" Enter your second fruits   ");
            b = Console.ReadLine();
            // Console.ReadLine();
            Console.Write(" Please Rate it   ");
            rate2 = Convert.ToInt32(Console.ReadLine());
            // Console.ReadLine();

            Console.Write(" Enter your third fruits   ");
            c = Console.ReadLine();
            // Console.ReadLine();
            Console.Write(" Please Rate it   ");
            rate3 = Convert.ToInt32(Console.ReadLine());
            //   Console.ReadLine();

            Console.Write(" Enter your fourth fruits   ");
            d = Console.ReadLine();
            // Console.ReadLine();
            Console.Write(" Please Rate it   ");
            rate4 = Convert.ToInt32(Console.ReadLine());
            // Console.ReadLine();


            Console.Write(" Enter your five fruits   ");
            e = Console.ReadLine();
            // Console.ReadLine();
            Console.Write(" Please Rate it   ");
            rate5 = Convert.ToInt32(Console.ReadLine());
            // Console.ReadLine();

            //if (String.ReferenceEquals(a , c))
            //    Console.WriteLine("a and c are interned.");

            //if (a == b)

            //    return false;
            //Console.ReadLine();

            //if (a==b && b==c && c==d && d==e)
            //{
            //    value = a;
            //    value = b;
            //    value = c;
            //    value = d;
            //    value = e;

            //    Console.Write("you have given same fruite twice: ");
            //}

            // If num is an odd number, throw an ArgumentException.
            //if (a == b && b==c)
            //{
            //    throw new ArgumentException(String.Format(" you have given same fruites twice"));



            //}

            var dictionary = new Dictionary<string, int>(5);
            dictionary.Add(a, rate);
            dictionary.Add(b, rate2);
            dictionary.Add(c, rate3);
            dictionary.Add(d, rate4);
            dictionary.Add(e, rate5);


            var items = from pair in dictionary
                        orderby pair.Value ascending
                        select pair;

            foreach (KeyValuePair<string, int> pair in items)
            {
                Console.WriteLine("{0}: {1}", pair.Key, pair.Value);

            }


            //items = from pair in dictionary
            //        orderby pair.Value descending
            //        select pair;

            Console.ReadLine();
        }
    }
}

